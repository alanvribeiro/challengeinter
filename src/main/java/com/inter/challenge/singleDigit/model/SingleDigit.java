package com.inter.challenge.singleDigit.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "single_digit")
public class SingleDigit {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
    private Long id;

    @Column
    private String strNumber;
    
    @Column
    private Integer qtyConcatenation;
    
    @Column
    private Integer result;
    
    @NotNull
	@ManyToOne
	@JoinColumn(name = "user_id")
    private User user;
	
}
