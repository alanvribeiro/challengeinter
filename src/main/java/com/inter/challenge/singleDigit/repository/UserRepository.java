package com.inter.challenge.singleDigit.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inter.challenge.singleDigit.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
