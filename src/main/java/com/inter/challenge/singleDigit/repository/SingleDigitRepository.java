package com.inter.challenge.singleDigit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.inter.challenge.singleDigit.model.SingleDigit;

public interface SingleDigitRepository extends JpaRepository<SingleDigit, Long> {
	
	@Query("SELECT s FROM SingleDigit s WHERE s.user.id = :idUser")
	public List<SingleDigit> findByUser(@Param("idUser") Long idUser);

}
