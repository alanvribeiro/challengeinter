package com.inter.challenge.singleDigit.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ParamSingleDigitDTO {
	
    private String strNumber;
    
    private Integer qtyConcatenation;
    
}
