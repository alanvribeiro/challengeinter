package com.inter.challenge.singleDigit.service;

import com.inter.challenge.singleDigit.dto.UserDTO;
import com.inter.challenge.singleDigit.dto.UserEncryptDTO;
import com.inter.challenge.singleDigit.model.User;

public interface CryptographyService {
	
	public String findPublicKey() throws Exception;
	
	public UserEncryptDTO encryptUserData(UserDTO userDTO, String strPublicKey) throws Exception;
	
	public User decryptUserData(UserEncryptDTO userEncryptDTO) throws Exception;
	
}
