package com.inter.challenge.singleDigit.service.impl;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.inter.challenge.singleDigit.dto.UserEncryptDTO;
import com.inter.challenge.singleDigit.error.BusinessRuleException;
import com.inter.challenge.singleDigit.model.User;
import com.inter.challenge.singleDigit.repository.UserRepository;
import com.inter.challenge.singleDigit.service.CryptographyService;
import com.inter.challenge.singleDigit.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	CryptographyService cryptographyService;

	@Override
	public List<User> findAll() {
		
		log.info("Listing all users.");
		
		return userRepository.findAll();
		
	}
	
	@Override
	public User findById(Long id) {
		
		log.info("Finding user id {}.", id);
		
		return userRepository.findById(id).orElseThrow(() -> new NoSuchElementException("User not found."));
		
	}

	@Override
	@Transactional
	public User create(UserEncryptDTO userEncryptDTO) throws Exception {
		
		User user = cryptographyService.decryptUserData(userEncryptDTO);
		
		validateUser(user);
		
		log.info("Creating the user {}.", user.getName());
		
		return userRepository.save(user);
		
	}

	@Override
	@Transactional
	public User update(User user) {
		
		if(user.getId() != null && findById(user.getId()) != null) {
			
			validateUser(user);
			
			log.info("Updating the user {}.", user.getName());
			
			return userRepository.save(user);
			
		}
		
		return null;
		
	}

	@Override
	@Transactional
	public void delete(Long id) {
		
		User user = findById(id);
		
		if(user != null) {
			
			log.info("Deleting the user {}.", user.getName());
			
			userRepository.delete(user);
			
		}
		
	}
	
	private void validateUser(User user) {
		
		StringBuilder sbRequiredFields = new StringBuilder();
		
		if(user.getName() == null || user.getName().equals("")) {
			sbRequiredFields.append("name");
		}
		
		if(user.getEmail() == null || user.getEmail().equals("")) {
			
			if(!sbRequiredFields.toString().equals("")) {
				sbRequiredFields.append(" and email");
			}
			else {
				sbRequiredFields.append("email");
			}
			
		}
		
		if(!sbRequiredFields.toString().equals("")){
			
			log.info("User fields not informed: {}", sbRequiredFields.toString());
			
			throw new BusinessRuleException("Mandatory fields: " + sbRequiredFields.toString() + ".");
			
		}
		else {
			log.info("User successfully verified.");
		}
		
	}
	
}
