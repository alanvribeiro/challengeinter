package com.inter.challenge.singleDigit.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Service;

import com.inter.challenge.singleDigit.dto.UserDTO;
import com.inter.challenge.singleDigit.dto.UserEncryptDTO;
import com.inter.challenge.singleDigit.model.User;
import com.inter.challenge.singleDigit.service.CryptographyService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CryptographyServiceImpl implements CryptographyService {
	
	private static final String ALGORITHM = "RSA";
	private static final String PATH_PRIVATE_KEY = System.getProperty("java.io.tmpdir") + "//private.key";
	private static final String PATH_PUBLIC_KEY = System.getProperty("java.io.tmpdir") + "//public.key";
	
	private void generateKeyPair() throws NoSuchAlgorithmException, IOException {

		log.info("Initializing key pair generation.");

		final KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(ALGORITHM);
		keyPairGenerator.initialize(2048);

		final KeyPair keyPair = keyPairGenerator.generateKeyPair();

		log.info("Creating public key.");

		File publicKeyFile = new File(PATH_PUBLIC_KEY);

		if (publicKeyFile.getParentFile() != null) {
			publicKeyFile.getParentFile().mkdirs();
		}

		publicKeyFile.createNewFile();

		ObjectOutputStream publicKeyOut = new ObjectOutputStream(new FileOutputStream(publicKeyFile));
		publicKeyOut.writeObject(keyPair.getPublic());
		publicKeyOut.close();

		log.info("Creating private key.");

		File privateKeyFile = new File(PATH_PRIVATE_KEY);

		if (privateKeyFile.getParentFile() != null) {
			privateKeyFile.getParentFile().mkdirs();
		}

		privateKeyFile.createNewFile();

		ObjectOutputStream privateKeyOut = new ObjectOutputStream(new FileOutputStream(privateKeyFile));
		privateKeyOut.writeObject(keyPair.getPrivate());
		privateKeyOut.close();

	}
	
	private boolean validateGeneratedKeyPair() {
		
		log.info("Checking the existence of created keys.");
		
		File publicKeyFile = new File(PATH_PUBLIC_KEY);
		File privateKeyFile = new File(PATH_PRIVATE_KEY);
		
		if (publicKeyFile.exists() && privateKeyFile.exists()) {
			return true;
		}
		
		return false;
		
	}
	
	private PublicKey getPublicKey() throws FileNotFoundException, IOException, ClassNotFoundException, NoSuchAlgorithmException {
		
		if(!validateGeneratedKeyPair()) {
			generateKeyPair();
		}
		
		log.info("Retrieving public key.");
		
		ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(PATH_PUBLIC_KEY));
		
		PublicKey publicKey = (PublicKey) inputStream.readObject();
		
		inputStream.close();
		
		return publicKey;
		
	}
	
	private PrivateKey getPrivateKey() throws Exception {
		
		if(!validateGeneratedKeyPair()) {
			generateKeyPair();
		}
		
		log.info("Retrieving private key.");
		
		ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(PATH_PRIVATE_KEY));
		
		PrivateKey privateKey = (PrivateKey) inputStream.readObject();
		
		inputStream.close();
		
		return privateKey;
		
	}
	
	private byte[] encrypts(String text, PublicKey publicKey) throws Exception {
		
		log.info("Encrypting data.");
		
		final Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		
		byte[] encryptedText = cipher.doFinal(text.getBytes());
		
		return encryptedText;
		
	}
	
	private String decrypts(byte[] text, PrivateKey privateKey) throws Exception {
		
		log.info("Decrypting data.");
		
		final Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		
		byte[] dectyptedText = cipher.doFinal(text);
		
		return new String(dectyptedText);
		
	}
	
	private PublicKey converterPublicKey(String strPublicKeyEncoded) throws Exception {
		
		log.info("Converting encoded to public key.");
		
		byte[] publicKeyEncoded = Base64.decodeBase64(strPublicKeyEncoded);
		
		KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
		
	    X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(publicKeyEncoded);
	    
	    return keyFactory.generatePublic(x509EncodedKeySpec);
		
	}
	
	@Override
	public String findPublicKey() throws Exception {
		
		log.info("Converting encoded public key to string.");
		
		PublicKey publicKey = getPublicKey();
		
		return Base64.encodeBase64String(publicKey.getEncoded());
		
	}
	
	@Override
	public UserEncryptDTO encryptUserData(UserDTO userDTO, String strPublicKey) throws Exception {
		
		PublicKey publicKey = converterPublicKey(strPublicKey);
		
		log.info("Encrypting user data.");
		
		UserEncryptDTO userEncryptDTO = UserEncryptDTO.builder()
				.nameEncrypt(
						encrypts(
								userDTO.getName(), 
								publicKey))
				.emailEncrypt(
						encrypts(
								userDTO.getEmail(), 
								publicKey))
				.build();
		
		log.info("Encrypted data: name {} and email {}.", userEncryptDTO.getNameEncrypt().toString(), userEncryptDTO.getEmailEncrypt().toString());
		
		return userEncryptDTO;
		
	}
	
	@Override
	public User decryptUserData(UserEncryptDTO userEncryptDTO) throws Exception {
		
		PrivateKey privateKey = getPrivateKey();
		
		log.info("Decrypting user data.");
		
		User user = User.builder()
				.name(
						decrypts(
								userEncryptDTO.getNameEncrypt(),
								privateKey))
				.email(
						decrypts(
								userEncryptDTO.getEmailEncrypt(), 
								privateKey))
				.build();
		
		log.info("Decrypted data: name {} and email {}.", user.getName(), user.getEmail());
		
		return user;
		
	}
	
}
