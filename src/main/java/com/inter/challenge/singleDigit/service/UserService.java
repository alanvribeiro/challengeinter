package com.inter.challenge.singleDigit.service;

import java.util.List;

import com.inter.challenge.singleDigit.dto.UserEncryptDTO;
import com.inter.challenge.singleDigit.model.User;

public interface UserService {
	
	public List<User> findAll();
	
	public User findById(Long id);

	public User create(UserEncryptDTO userEncryptDTO) throws Exception;
	
	public User update(User user);
	
	public void delete(Long id);

}
