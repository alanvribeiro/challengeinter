package com.inter.challenge.singleDigit.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.inter.challenge.singleDigit.dto.ParamSingleDigitDTO;
import com.inter.challenge.singleDigit.dto.SingleDigitDTO;
import com.inter.challenge.singleDigit.error.BusinessRuleException;
import com.inter.challenge.singleDigit.model.SingleDigit;
import com.inter.challenge.singleDigit.repository.SingleDigitRepository;
import com.inter.challenge.singleDigit.service.SingleDigitService;
import com.inter.challenge.singleDigit.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SingleDigitServiceImpl implements SingleDigitService {
	
	private static Map<ParamSingleDigitDTO, SingleDigitDTO> mapCacheSingleDigit = new HashMap<>();
	
	@Autowired
	SingleDigitRepository singleDigitRepository;
	
	@Autowired
	UserService userService;
	
	@Override
	public List<SingleDigit> findByUser(Long idUser) {
		return singleDigitRepository.findByUser(idUser);
	}
	
	@Override
	@Transactional
	public SingleDigit create(SingleDigit singleDigit) {
		
		validateLinkedUser(singleDigit);
		
		singleDigit = validateSingleDigitCalculation(singleDigit);
		
		log.info("Creating single digit data for {}.", singleDigit.getStrNumber());
		
		return singleDigitRepository.save(singleDigit);
		
	}
	
	private void validateLinkedUser(SingleDigit singleDigit) {
		
		log.info("Validating linked user.");
		
		if(singleDigit.getUser() != null && singleDigit.getUser().getId() != null) {
			userService.findById(singleDigit.getUser().getId());
		}
		
	}
	
	private SingleDigit validateSingleDigitCalculation(SingleDigit singleDigit) {
		
		log.info("Validating the single digit calculation for {}.", singleDigit.getStrNumber());
		
		if(singleDigit.getResult() == null) {
			singleDigit.setResult(cacheCalculateSingleDigit(singleDigit.getStrNumber(), singleDigit.getQtyConcatenation()));
		}
		
		return singleDigit;
	}
	
	@Override
	public Integer cacheCalculateSingleDigit(String strNumber, Integer qtyConcatenation) {
		
		log.info("Initializing single digit cache.");
		
		ParamSingleDigitDTO keyParameters = ParamSingleDigitDTO.builder().strNumber(strNumber).qtyConcatenation(qtyConcatenation).build();
		
		if(mapCacheSingleDigit.containsKey(keyParameters)){
			
			log.info("Retrieving the cache calculation.");
			
			return mapCacheSingleDigit.get(keyParameters).getResult();
			
		}
		else {
			
			log.info("Initializing the single digit calculation.");
			
			if(mapCacheSingleDigit.values().size() == 10) {
				
				for(Map.Entry<ParamSingleDigitDTO, SingleDigitDTO> entry : mapCacheSingleDigit.entrySet()) {
					mapCacheSingleDigit.remove(entry.getKey());
					break;
		    	} 
				
			}
			
			Integer singleDigit = calculateSingleDigit(strNumber, qtyConcatenation);
			
			SingleDigitDTO singleDigitDTO = SingleDigitDTO.builder().parameters(keyParameters).result(singleDigit).datCalculation(new Date()).build();
			
			mapCacheSingleDigit.put(keyParameters, singleDigitDTO);
			
			return singleDigit;
			
		}
		
	}
	
	private Integer calculateSingleDigit(String strNumber, Integer qtyConcatenation) {
		
		String strNumberConcatenated = calculateNumberConcatenated(strNumber, qtyConcatenation);
		
		return calculateSingleDigit(strNumberConcatenated);
		
	}
	
	private String calculateNumberConcatenated(String strNumber, Integer qtyConcatenation) {
		
		if(strNumber != null && !strNumber.equals("") && qtyConcatenation != null) {
			
			log.info("Concatenating the number {} in {} times.", strNumber, qtyConcatenation);
			
			StringBuilder sbNumberConcatenated = new StringBuilder();
			
			for(int i = 0; i < qtyConcatenation; i++) {
				sbNumberConcatenated.append(strNumber);
			}
			
			return sbNumberConcatenated.toString();
			
		}
		
		log.warn("Unable to perform concatenation.");
		
		throw new BusinessRuleException("Unable to perform concatenation.");
		
	}
	
	private Integer calculateSingleDigit(String strNumberConcatenated) {
		
		if(strNumberConcatenated.length() == 1) {
			
			log.info("The result of the single digit is {}.", strNumberConcatenated);
			
			return Integer.parseInt(strNumberConcatenated);
			
		}
		else {
			
			Integer result = 0;
			
			for(char number : strNumberConcatenated.toCharArray()) {
				result += Character.getNumericValue(number);
			}
			
			return calculateSingleDigit(result.toString());
			
		}
		
	}
	
}
