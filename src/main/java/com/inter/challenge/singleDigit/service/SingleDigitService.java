package com.inter.challenge.singleDigit.service;

import java.util.List;

import com.inter.challenge.singleDigit.model.SingleDigit;

public interface SingleDigitService {
	
	public List<SingleDigit> findByUser(Long idUser);
	
	public SingleDigit create(SingleDigit singleDigit); 
	
	public Integer cacheCalculateSingleDigit(String strNumber, Integer qtyConcatenation);
	
}
