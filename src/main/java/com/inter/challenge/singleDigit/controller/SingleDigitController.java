package com.inter.challenge.singleDigit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inter.challenge.singleDigit.model.SingleDigit;
import com.inter.challenge.singleDigit.service.SingleDigitService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api/singleDigit")
public class SingleDigitController {
	
	@Autowired
	SingleDigitService singleDigitService;
	
	@GetMapping("/findByUser/{idUser}")
	@ApiOperation(value = "Search list of single digit by user id", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<SingleDigit>> findByUser(
			@PathVariable("idUser") @ApiParam(value="User id", required = true, name="idUser") final Long idUser) {
		
		return ResponseEntity.ok().body(singleDigitService.findByUser(idUser));
		
	}
	
	@PostMapping("/create")
	@ApiOperation(value = "Create single digit", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<SingleDigit> create(
			@RequestBody @ApiParam(value="Data to include a single digit", required = true, name="singleDigit") final SingleDigit singleDigit) {
		
		return ResponseEntity.ok().body(singleDigitService.create(singleDigit));
		
	}
	
	@GetMapping("/calculateSingleDigit")
	@ApiOperation(value = "Calculate single digit", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> calculateSingleDigit(
			@RequestParam("strNumber") @ApiParam(value="String representing the integer", required = true, name="strNumber") final String strNumber,
			@RequestParam("qtyConcatenation") @ApiParam(value="Integer representing the number of times the concatenation", required = true, name="qtyConcatenation") final Integer qtyConcatenation) {
		
		return ResponseEntity.ok().body(singleDigitService.cacheCalculateSingleDigit(strNumber, qtyConcatenation));
		
	}
	
}
