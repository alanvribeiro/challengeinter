package com.inter.challenge.singleDigit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inter.challenge.singleDigit.dto.UserDTO;
import com.inter.challenge.singleDigit.dto.UserEncryptDTO;
import com.inter.challenge.singleDigit.model.User;
import com.inter.challenge.singleDigit.service.CryptographyService;
import com.inter.challenge.singleDigit.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api/user")
public class UserController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	CryptographyService cryptographyService;
	
	@GetMapping("/findAll")
	@ApiOperation(value = "Search all users", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<User>> findAll() {
		
		return ResponseEntity.ok().body(userService.findAll());
		
	}

	@GetMapping("/findById/{id}")
	@ApiOperation(value = "Search user by id", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> findById(
			@PathVariable("id") @ApiParam(value="User id", required = true, name="id") final Long id) throws Exception {
		
		return ResponseEntity.ok().body(userService.findById(id));
		
	}
	
	@PostMapping("/create")
	@ApiOperation(value = "Add a user", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> create(
			@RequestHeader("publicKey") final String publicKey,
			@RequestBody @ApiParam(value="Data to include a user", required = true, name="user") final UserDTO userDTO) throws Exception {
		
		UserEncryptDTO userEncryptDTO = cryptographyService.encryptUserData(userDTO, publicKey);
		
		return ResponseEntity.ok().body(userService.create(userEncryptDTO));
		
	}
	
	@PutMapping("/update")
	@ApiOperation(value = "Edit a user", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> update(
			@RequestBody @ApiParam(value="Data to edit a user", required = true, name="user") final User user) throws Exception {
		
		return ResponseEntity.ok().body(userService.update(user));
		
	}
	
	@DeleteMapping("/delete/{id}")
	@ApiOperation(value = "Delete a user")
	public ResponseEntity<Object> delete(
			@PathVariable("id") @ApiParam(value="User id", required = true, name="id") final Long id) throws Exception {
		
		userService.delete(id);
		return ResponseEntity.ok().body("User deleted.");
		
	}
	
}
