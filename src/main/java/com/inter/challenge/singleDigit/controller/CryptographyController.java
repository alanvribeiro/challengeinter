package com.inter.challenge.singleDigit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inter.challenge.singleDigit.service.CryptographyService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/cryptography")
public class CryptographyController {
	
	@Autowired
	CryptographyService cryptographyService;
	
	@GetMapping("/findPublicKey")
	@ApiOperation(value = "Find the public key to encrypt data", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> findPublicKey() throws Exception {
		
		return ResponseEntity.ok().body(cryptographyService.findPublicKey());
		
	}
	
}
