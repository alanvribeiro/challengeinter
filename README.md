# Desafio do Banco Inter
O desafio é composto de quatro partes principais: cálculo de dígito único, CRUD de usuário, cache em memória e criptografia.
## Linguagem e frameworks utilizados
> Java 8

> Spring Boot

> Lombok

> H2 Database

> Swagger

## Endpoints
> APIs de Usuário

```
GET http://localhost:8080/api/user/findAll
```
```
GET http://localhost:8080/api/user/findById/{id}
```
```
POST http://localhost:8080/api/user/create
```
```
PUT http://localhost:8080/api/user/update
```
```
DEL http://localhost:8080/api/user/delete/{id}
```

> APIs de Dígito Único

```
GET http://localhost:8080/api/singleDigit/findByUser/{id}
```
```
POST http://localhost:8080/api/singleDigit/create
```
```
GET http://localhost:8080/api/singleDigit/calculateSingleDigit?strNumber={strNumber}&qtyConcatenation={qtyConcatenation}
```

> API de Criptografia

```
GET http://localhost:8080/api/cryptography/findPublicKey
```

## Informações
> Compilando e executando a aplicação

```
mvn spring-boot:run
```

> Executando os testes unitários

```
mvn test
```

> Observações

```
1. Foi utilizado HashMap para a estratégia de cache em meória.

2. A criptografia de descriptografia dos dados do usuário foi demonstrada nas camadas de "controller" e "service", respectivamente. Pra demosntrar sua execução, a apliação registra logs dos dados antes e depois do chaveamento.
```





